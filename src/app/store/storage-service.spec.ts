import {TestBed} from '@angular/core/testing';
import {StorageService} from './storage.service';
import {BrowserStorageService} from './browser-storage.service';
import {ConstantsService} from '../global/constants.service';
import {IPersonDetail} from '../details/details.service';


describe('test store data', () => {

  const mockBrowserStorageService = jasmine.createSpyObj(['getItem', 'setItem']);
  let storageService: StorageService;
  const mockConstantService = new ConstantsService();
  const mockFavorities = [
    {
      id: 1,
      type: 'mytype1',
      header: 'header1',
      poster: null,
      description: 'description1',
      show: true,
      adult: false
    },
    {
      id: 2,
      type: 'mytype2',
      header: 'header2',
      poster: null,
      description: 'description2',
      show: false,
      adult: true
    }
  ];

  beforeEach(() => {


    TestBed.configureTestingModule( {
        providers: [
          StorageService,
          {provide: BrowserStorageService, useValue: mockBrowserStorageService},
          {provide: ConstantsService, useValue: mockConstantService}
        ]

      });
    storageService = TestBed.inject(StorageService);
    mockConstantService.user =  {
      id: 1,
      userName: 'Stephen',
      password: 'pass'
    };
  });

  it('should be able to get stored favorites', () => {
    mockBrowserStorageService.getItem.and.returnValue(JSON.stringify(mockFavorities));

    const storedFavorites = storageService.getStoredFavorites();
    expect(storedFavorites[0].id).toBe(mockFavorities[0].id);
    expect(storedFavorites[1].id).toBe(mockFavorities[1].id);
    expect(storedFavorites[0].type).toBe(mockFavorities[0].type);
    expect(storedFavorites[1].type).toBe(mockFavorities[1].type);
    expect(storedFavorites[0].show).toBe(mockFavorities[0].show);
    expect(storedFavorites[1].show).toBe(mockFavorities[1].show);

  });

  it('should be able to get stored favorites from behavior', (done) => {
    mockBrowserStorageService.getItem.and.returnValue(JSON.stringify(mockFavorities));
    storageService.getStoredFavorites();
    storageService.favorites$.subscribe(data => {
      expect(data.length).toBe(mockFavorities.length);
      expect(data[0].id).toBe(mockFavorities[0].id);
      expect(data[1].id).toBe(mockFavorities[1].id);
      expect(data[0].type).toBe(mockFavorities[0].type);
      expect(data[1].type).toBe(mockFavorities[1].type);
      expect(data[0].show).toBe(mockFavorities[0].show);
      expect(data[1].show).toBe(mockFavorities[1].show);
      done();
    });
  });

  it('should be a able to store a favorite', (done) => {
    storageService.setStoredFavorites(mockFavorities);
    storageService.favorites$.subscribe(data => {
      expect(data.length).toBe(mockFavorities.length);
      expect(data[0].id).toBe(mockFavorities[0].id);
      expect(data[1].id).toBe(mockFavorities[1].id);
      expect(data[0].type).toBe(mockFavorities[0].type);
      expect(data[1].type).toBe(mockFavorities[1].type);
      expect(data[0].show).toBe(mockFavorities[0].show);
      expect(data[1].show).toBe(mockFavorities[1].show);
      done();
    });
  });

  it('should be able to determine if favorite is already stored', () => {
    mockBrowserStorageService.getItem.and.returnValue(JSON.stringify(mockFavorities));
    const hasFavorite = storageService.hasStoredFavorite(mockFavorities[0]);
    expect(hasFavorite).toBeTruthy();
  });

  it('should be able to determine if favorite is already stored', () => {
    mockBrowserStorageService.getItem.and.returnValue(JSON.stringify(mockFavorities));
    const hasFavorite = storageService.hasStoredFavorite(mockFavorities[0]);
    expect(hasFavorite).toBeTruthy();
  });

  it('should be able to remove favorite', (done) => {
    mockBrowserStorageService.getItem.and.returnValue(JSON.stringify(mockFavorities));
    storageService.removeFavorite(mockFavorities[0]);
    storageService.favorites$.subscribe(data => {
      expect(data.length).toBe(mockFavorities.length - 1);
      done();
    });
  });

  it('should be able to add a movie', (done) => {
    mockBrowserStorageService.getItem.and.returnValue(JSON.stringify(mockFavorities));
    const mockMovie = {
      id: 5,
      title: 'myTitle1',
      adult: false,
      overview: 'overview1',
      homepage: 'homepage1',
      backdrop_path: 'backdrop_path1',
      poster_path: 'poster_path1',
      production_companies: [],
      release_date: 'release_date1'
    };
    storageService.addMovie(mockMovie);
    storageService.favorites$.subscribe(data => {
      expect(data.length).toBe(3);
      expect(data[data.length - 1].id).toBe(mockMovie.id);
      done();
    });

  });

  it('should be able to add a person', (done) => {
    mockBrowserStorageService.getItem.and.returnValue(JSON.stringify(mockFavorities));
    const mockPerson: IPersonDetail = {
      adult: true,
      also_known_as: [],
      biography: 'bio',
      birthday: 'birthday',
      deathday: 'deathday',
      gender: 0,
      homepage: 'homepage',
      id: 10,
      imdb_id: 'bio',
      known_for_department: 'known_for_department',
      name: 'name',
      place_of_birth: 'place_of_birth',
      popularity: 2.345,
      profile_path: 'profile_path',
      credit: null,
      show: true
    };
    storageService.addPerson(mockPerson);
    storageService.favorites$.subscribe(data => {
      expect(data.length).toBe(3);
      expect(data[data.length - 1].id).toBe(mockPerson.id);
      done();
    });
  });



});
