import {Injectable} from '@angular/core';
import {ConstantsService, IFavorite} from '../global/constants.service';
import {BehaviorSubject} from 'rxjs';
import {IMovieDetails, IPersonDetail} from '../details/details.service';
import {BrowserStorageService} from './browser-storage.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService  {
  constructor(private constants: ConstantsService, private browserStorage: BrowserStorageService) { }

  favorites$ = new BehaviorSubject<IFavorite[]>([]);

  private static movieObj(movie: IMovieDetails): IFavorite {
    return {id: movie.id, header: movie.title, poster: movie.poster_path, description: movie.overview, adult: movie.adult, type: 'movie'};
  }

  private static personObj(person: IPersonDetail): IFavorite {
    return {id: person.id, header: person.name, poster: person.profile_path,
      description: person.biography, adult: person.adult, type: 'person'};
  }



  public getStoredFavorites(): IFavorite[] {
    let retval =  [];
    if (this.constants.user != null) {
      const json =  this.browserStorage.getItem(this.constants.STORED_MOVIE_IDS + this.constants.user);
      const movies: IFavorite[] = JSON.parse(json);
      if (movies != null) {
        movies.sort((item1, item2) => (item1.header > item2.header ? 1 : -1));
        this.favorites$.next(movies);
        retval = json == null ? [] : movies;
      }
    }
    return retval;
  }

  public setStoredFavorites(movies: IFavorite[]) {
    this.favorites$.next(movies);
    this.browserStorage.setItem(this.constants.STORED_MOVIE_IDS + this.constants.user, JSON.stringify(movies));
  }

  public addMovie(movie: IMovieDetails) {
    const iMovieFavorite = StorageService.movieObj(movie);
    this.addFavorite(iMovieFavorite);
  }

  public addPerson(person: IPersonDetail) {
    const iMovieFavorite = StorageService.personObj(person);
    this.addFavorite(iMovieFavorite);
  }

  private addFavorite(iMovieFavorite: IFavorite) {
    if (!this.hasStoredFavorite(iMovieFavorite)) {
      const storedMovies = this.getStoredFavorites();
      storedMovies.push(iMovieFavorite);
      this.setStoredFavorites(storedMovies);
    }
  }

  public removeFavorite(detail: IMovieDetails | IPersonDetail) {
    const iMovieFavorite = StorageService.movieObj(detail);
    if (this.hasStoredFavorite(iMovieFavorite)) {
      const storedMovies = this.getStoredFavorites();
      storedMovies.splice(storedMovies.findIndex(o => o.id === detail.id), 1);
      this.setStoredFavorites(storedMovies);
    }
  }


  public hasStoredFavorite(movie: IFavorite): boolean {
    return this.getFavorite(movie) != null;
  }

  private getFavorite(movie: IFavorite) {
    let favorite = null;
    const storedFavorites = this.getStoredFavorites();
    if (storedFavorites != null) {
      favorite = storedFavorites.find(o => o.id === movie.id);
    }
    return favorite;
  }


}

