import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BrowserStorageService {

  constructor() { }

  getItem(item: string) {
    return  window.localStorage.getItem(item);
  }

  setItem(idItem: string, item: string) {
    window.localStorage.setItem(idItem, item);
  }
}
