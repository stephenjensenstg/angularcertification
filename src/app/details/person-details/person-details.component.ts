import {Component, OnInit} from '@angular/core';
import {DetailsService, ICast, ICrew, IPersonDetail} from '../details.service';
import {ActivatedRoute} from '@angular/router';
import {ConstantsService} from '../../global/constants.service';
import {StorageService} from '../../store/storage.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss']
})
export class PersonDetailsComponent implements OnInit {


  panelOpenState = false;
  selectCredit = '';
  constructor(private detailService: DetailsService,
              private storage: StorageService,
              private constants: ConstantsService,
              private router: ActivatedRoute) { }

  public personObservable: Observable<IPersonDetail>;

  ngOnInit() {
    this.router.params.subscribe(param => {
      this.personObservable = this.detailService.getPerson(param.personId).pipe(
        map(data => {
          data.credit.cast = this.getCast(data.credit.cast);
          data.credit.crew = this.getCrew(data.credit.crew);
          return data;
        })
      );
    });
  }

  backClicked() {
    window.history.back();
  }

  getCast(cast: ICast[]) {
    if (cast != null) {
        cast = cast.sort((a, b) => (new Date(b.release_date)).getTime() - (new Date(a.release_date)).getTime());  // Sort is wrong
        cast = cast.splice(0, this.constants.numberOfCastingCount);
    }
    return cast;
  }

  private getCrew(crew: ICrew[]) {
    if (crew != null) {
      crew = crew.sort((a, b) => (new Date(b.release_date)).getTime() - (new Date(a.release_date)).getTime());  // Sort is wrong
      crew = crew.splice(0, this.constants.numberOfCrewCount);
    }
    return crew;
  }
}
