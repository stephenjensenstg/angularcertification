import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PersonDetailsComponent } from './person-details.component';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {DetailsService, IPersonDetail} from '../details.service';
import {BrowserModule, By} from '@angular/platform-browser';
import {MaterialModule} from '../../global/materials-module';
import {FormsModule} from '@angular/forms';
import {AddButtonComponent} from '../common/add-button/add-button.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('PersonDetailsComponent', () => {
  let component: PersonDetailsComponent;
  let fixture: ComponentFixture<PersonDetailsComponent>;
  const PERSON_ID = 1;
  const spyDetailService = jasmine.createSpyObj('DetailsService', ['getPerson']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonDetailsComponent, AddButtonComponent ],
      imports: [
        RouterTestingModule,
        MaterialModule,
        BrowserModule,
        FormsModule
      ],
      providers: [
        {provide: ActivatedRoute, useValue: {
          params: of({personId: PERSON_ID })
        }},
        {
          provide: DetailsService, useValue: spyDetailService
        }
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDetailsComponent);
    component = fixture.componentInstance;
    spyDetailService.getPerson.and.returnValue(of(json));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render persons name', done => {
    fixture.componentInstance.personObservable.subscribe( data => {
        console.log(data);
        expect(data.name).toBe(json.name);
        const name = fixture.debugElement.query(By.css('.person_name'));
        expect(name.nativeElement.textContent).toContain(json.name);
        done();
      }
    );
  });

  it('should truncate bio', done => {
    fixture.componentInstance.personObservable.subscribe( data => {
      const name = fixture.debugElement.query(By.css('.biography'));
      expect(name.nativeElement.textContent.length).toBe(1);  // Should not have data at first
      done();
    });
  });

  const json: IPersonDetail = {
      name: 'John Doe',
      popularity: 2.2,
      biography: '',
      credit:  {
            cast: [],
            crew: []
        },
      adult: false
    };

});
