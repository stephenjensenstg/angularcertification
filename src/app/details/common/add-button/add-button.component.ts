import {Component, Input} from '@angular/core';
import {IMovieDetails, IPersonDetail} from '../../details.service';
import {ConstantsService, IFavorite} from '../../../global/constants.service';
import {StorageService} from '../../../store/storage.service';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.scss']
})
export class AddButtonComponent  {

  constructor(private constants: ConstantsService, private  storage: StorageService) { }

  // #Input is how you pass parameter values into a component.
  @Input()
  item: IMovieDetails | IPersonDetail;

  @Input()
  type: string;

  // #Output  The component that is using this component can send a method.  That method will get called if we emit()
  //   with our output variable.  myVar.emit()   We can also send up values  myVar.emit(somthing);
  //                  This is called event handling.
  //                  Situation:  You are using a component that has @Output variable.  This is an event that will occur
  //                  within this component.   You need a certain action to occur  when this event occurs
  //                              so you provide your method   <app-add-button  (myVar)='theEventIwantRan'/>
  // @Output() myVar = new EventEmitter;

  isLoggedIn(): boolean {
    return this.constants.user != null;
  }

  addItem(item: IMovieDetails | IPersonDetail) {
    if (this.type === 'movie') {
      this.storage.addMovie(item);
    } else if (this.type === 'person') {
      this.storage.addPerson(item);
    } else {
      throw new Error('type not exist');
    }
  }

  removeItem(item: IMovieDetails | IPersonDetail) {
    this.storage.removeFavorite(item);
  }

  public hasStoredItem(item: IFavorite): boolean {
    return this.storage.hasStoredFavorite(item);
  }

}
