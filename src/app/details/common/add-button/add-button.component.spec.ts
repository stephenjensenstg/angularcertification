import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AddButtonComponent} from './add-button.component';
import {MaterialModule} from '../../../global/materials-module';
import {ConstantsService} from '../../../global/constants.service';
import {StorageService} from '../../../store/storage.service';
import {IMovieDetails, IPersonDetail} from '../../details.service';

describe('Add remove button test', () => {
  let fixture: ComponentFixture<AddButtonComponent>;
  let instance: AddButtonComponent;
  const spyStorageService = jasmine.createSpyObj(['addMovie', 'addPerson', 'removeFavorite', 'hasStoredFavorite' ]);
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AddButtonComponent
      ],
      imports: [
        MaterialModule

      ],
      providers: [
        ConstantsService,
        {provide: StorageService, useValue: spyStorageService}
      ]
    });
    fixture = TestBed.createComponent(AddButtonComponent);
    instance = fixture.componentInstance;
  });

  it('should be able to create component', () => {
    expect(fixture).toBeTruthy();
  });

  it('should call addMovie if type is movie', () => {
    instance.type = 'movie';
    fixture.detectChanges();
    const item: IMovieDetails = { id: 15};
    instance.addItem(item);
    expect(spyStorageService.addMovie).toHaveBeenCalled();
  });

  it('should call addPerson if type is person', () => {
    instance.type = 'person';
    fixture.detectChanges();
    const item: IPersonDetail = { id: 16};
    instance.addItem(item);
    expect(spyStorageService.addPerson).toHaveBeenCalled();
  });

  it('should call service to removeFavorite', () => {
    instance.removeItem({});
    expect(spyStorageService.removeFavorite).toHaveBeenCalled();
  });

  it('should call service to hasStoredFavorite', () => {
    instance.hasStoredItem({});
    expect(spyStorageService.hasStoredFavorite).toHaveBeenCalled();
  });



});
