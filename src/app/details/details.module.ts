import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import { PersonDetailsComponent } from './person-details/person-details.component';
import {MaterialModule} from '../global/materials-module';
import {FormsModule} from '@angular/forms';
import { AddButtonComponent } from './common/add-button/add-button.component';

@NgModule({
  declarations: [MovieDetailsComponent, PersonDetailsComponent, AddButtonComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
        {path: 'details/movie/:movieId', component: MovieDetailsComponent},
        {path: 'details/person/:personId', component: PersonDetailsComponent},
      ]
    ),
    MatIconModule,
    MaterialModule,
    FormsModule,

  ],
  exports: [RouterModule]
})
export class DetailsModule { }
