import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

// #Interface  Examples used for data.
export interface IProductionCompany {
  id?: number;
  logo_path?: string;
  name?: string;
  origin_country?: string;
}



export interface IMovieDetails {
  id?: number;
  title?: string;
  adult?: boolean;
  overview?: string;
  homepage?: string;
  backdrop_path?: string;
  poster_path?: string;
  production_companies?: IProductionCompany[];
  release_date?: string;
}

export interface IPersonDetail {
  adult?: boolean;
  also_known_as?: [];
  biography?: string;
  birthday?: string;
  deathday?: string;
  gender?: number;
  homepage?: string;
  id?: number;
  imdb_id?: string;
  known_for_department?: string;
  name?: string;
  place_of_birth?: string;
  popularity?: number;
  profile_path?: string;
  credit?: ICredits;
  show?: boolean;
}

export interface ICast {
  id?: number;
  video?: boolean;
  vote_count?: number;
  vote_average?: number;
  title?: string;
  release_date?: string;
  original_language?: string;
  original_title?: string;
  genre_ids?: [];
  backdrop_path?: string;
  adult?: boolean;
  overview?: string;
  poster_path?: string;
  popularity?: number;
  character?: string;
  credit_id?: string;
  order?: number;
  media_type?: string;
}

export interface ICrew {
  backdrop_path?: string;
  title?: string;
  genre_ids?: [];
  original_language?: string;
  original_title?: string;
  poster_path?: string;
  video?: boolean;
  vote_average?: number;
  overview?: string;
  id?: number;
  vote_count?: number;
  release_date?: string;
  adult?: boolean;
  popularity?: number;
  credit_id?: string;
  department?: string;
  job?: string;
  media_type?: string;
}
export interface ICredits {
  cast: ICast[];
  crew: ICrew[];
}






@Injectable({
  providedIn: 'root'
})
export class DetailsService {

  constructor(private http: HttpClient) { }

  private apiMovie = 'https://api.themoviedb.org/3/movie';
  private apiPerson = 'https://api.themoviedb.org/3/person';

  public getMovies(id: number ): Observable<IMovieDetails> {
    const url = `${this.apiMovie}/${id}`;
    return this.http.get<IMovieDetails>(url);
  }

  getPerson(id: number): Observable<IPersonDetail> {
    const main = this.getPersonMain(id);
    const credits = this.getPersonCredits(id);
    return forkJoin(main, credits).pipe(
      map(data => {
        const person  = data[0];
        person.credit = data[1];
        return person;
      })
    );
  }

  getPersonMain(id: number): Observable<IPersonDetail> {
    const url = `${this.apiPerson}/${id}`;
    return this.http.get<IPersonDetail>(url);
  }

  getPersonCredits(id: number): Observable<ICredits> {
    const url = `${this.apiPerson}/${id}/combined_credits`;
    return this.http.get<ICredits>(url);
  }

}
