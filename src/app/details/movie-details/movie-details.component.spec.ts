import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MovieDetailsComponent } from './movie-details.component';
import {MatIconModule} from '@angular/material/icon';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, of} from 'rxjs';
import {DetailsService} from '../details.service';
import {MockMovieDetailService} from '../../mocks/mock-movie-detail-service';
import {StorageService} from '../../store/storage.service';
import {IFavorite} from '../../global/constants.service';
import {AddButtonComponent} from '../common/add-button/add-button.component';

describe('MovieDetailsComponent', () => {
  let component: MovieDetailsComponent;
  let fixture: ComponentFixture<MovieDetailsComponent>;
  const MOVIE_ID = 1234;
  const spyMovieDetailService = jasmine.createSpyObj(['getMovies']);
  const TEST_TITLE = 'Gone with the Wind';
  const testMock = MockMovieDetailService.getMock(MOVIE_ID, TEST_TITLE);
  let favoritesList: IFavorite[] = [];
  const mockStorageService = {
    favorites$: new BehaviorSubject<IFavorite[]>(favoritesList),
    hasStoredFavorite: () => true,
    getStoredFavorites: (): IFavorite[] => {
      return favoritesList;
    },
    setStoredMovies: (movies: IFavorite[]) => {
      favoritesList = movies;
    },
    hasStoredMovie: (movie: IFavorite) => {
      return false;
    }
  };

  beforeEach(waitForAsync(() => {
    favoritesList = [];
    TestBed.configureTestingModule({
      declarations: [ MovieDetailsComponent, AddButtonComponent ],
      imports: [ MatIconModule

      ],
      providers: [
        {provide: ActivatedRoute, useValue: {
              params: of({movieId: MOVIE_ID })
          }},
        {provide: StorageService, useValue: mockStorageService},
        {provide: DetailsService, useValue: spyMovieDetailService},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailsComponent);
    component = fixture.componentInstance;
    spyMovieDetailService.getMovies.and.returnValue(of(testMock));
    fixture.detectChanges();
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call service to get favorite movies on load', (done) => {
    component.movie$.subscribe(data => {
      expect(spyMovieDetailService.getMovies).toHaveBeenCalled();
      done();
    });
  });

  it('should populate movie observer on load', (done) => {
    component.movie$.subscribe(data => {
      expect(data.id).toBe(MOVIE_ID);
      expect(data.title).toBe(TEST_TITLE);
      expect(data.production_companies.length).toBe(testMock.production_companies.length);
      done();
    });
  });

  it('should initially render the header of the movie with a button to add', (done) => {
    component.movie$.subscribe(data => {
      expect(fixture.nativeElement.querySelector('.detail_page').textContent).toContain(TEST_TITLE);
      expect(fixture.nativeElement.querySelector('.detail_page').textContent)
        .toContain(MockMovieDetailService.getMock(1, '1').production_companies[0].name);
      expect(fixture.nativeElement.querySelector('.detail_page').textContent)
        .toContain(MockMovieDetailService.getMock(1, '1').production_companies[1].name);
      done();
    });
  });

});
