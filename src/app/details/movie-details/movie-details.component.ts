import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IMovieDetails, DetailsService} from '../details.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnInit {
  movie$: Observable<IMovieDetails>;

  constructor(private router: ActivatedRoute,
              private detailService: DetailsService
              ) {
  }

  ngOnInit() {
    this.router.params
      .subscribe(param => {
          // #this this refers to the containing object and access to its fields and other methods.
          // Note: If this was straight java script and I did function <some function name> {  The this would refer to the
          //    function and not that which contains it.
          //   However I use the fat arrow   <function name> = () => { .... }  Then the this variable would refer to the
          //     field variable of what contains it like it is doing here.
          this.movie$ = this.detailService.getMovies(param.movieId);
        }
      );
  }



  backClicked() {
    window.history.back();
  }

}
