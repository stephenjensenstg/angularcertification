import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AuthModule} from './auth/auth.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {SearchModule} from './search/search.module';
import {MatMenuModule} from '@angular/material/menu';
import {AppRouterModule} from './app-router.module';
import {httpInterceptorProviders} from './http-provider';
import {DetailsModule} from './details/details.module';

// #Module Its like the glue that puts everything together.  It ties you components, services, pipes other modules together.
//   You could say certain functionality that you want you would tie together in a module.  When you want that functionality
//   you would bring in the module.  Which is called loading a module.
//   If there is certain functionality you do not want unless used then you would not load in that module until it is used.
//   Called lazy loading.
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    SearchModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AuthModule,
    BrowserAnimationsModule,
    MatDividerModule,
    MatButtonModule,
    MatMenuModule,
    AppRouterModule,
    DetailsModule
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
