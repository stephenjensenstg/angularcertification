import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AuthService} from './auth/auth.service';
import {ConstantsService} from './global/constants.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements  OnInit {

  constructor() {
  }

  ngOnInit(): void {

  }


}
