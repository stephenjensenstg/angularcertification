"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var HttpInterceptorService = /** @class */ (function () {
    function HttpInterceptorService() {
    }
    HttpInterceptorService_1 = HttpInterceptorService;
    HttpInterceptorService.prototype.intercept = function (req, next) {
        var apiAddedReq = req.clone({
            params: req.params.set('api_key', HttpInterceptorService_1.API_KEY)
        });
        return next.handle(apiAddedReq);
    };
    var HttpInterceptorService_1;
    HttpInterceptorService.API_KEY = '88ae69e5b2e07ce2ce99a82e20b13732';
    HttpInterceptorService = HttpInterceptorService_1 = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], HttpInterceptorService);
    return HttpInterceptorService;
}());
exports.HttpInterceptorService = HttpInterceptorService;
//# sourceMappingURL=http-interceptor.service.js.map