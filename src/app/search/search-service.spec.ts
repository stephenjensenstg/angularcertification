import {TestBed} from '@angular/core/testing';
import { SerachService } from './serach.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AppModule} from '../app.module';
import {of} from 'rxjs';

describe('Search Service can be created', () => {


  const mockHttpService = jasmine.createSpyObj(['get']);

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule, AppModule],
      providers: [{
         provide:  HttpClient,
         useValue: mockHttpService
        }
      ]
    })
  );



  it('should call http service and get data from subject', (done) => {
    const query = 'george';
    mockHttpService.get.and.returnValue(of(dataResultJson));
    const service: SerachService = TestBed.inject(SerachService);
    service.searchMovies(query, '2006', '1');
    expect(mockHttpService.get).toHaveBeenCalled();
    service.movies.subscribe(data => {
      if (data.searchInfo.total > 0) {
        expect(data.movieResults[0].id).toBe(dataResultJson.results[0].id);
        expect(data.movieResults[0].title).toBe(dataResultJson.results[0].title);
        expect(data.movieResults[0].overview).toBe(dataResultJson.results[0].overview);
        done();
      }
    });
  });


  const dataResultJson = {
    page: 1,
    results: [
      {
        id: 26264,
        overview: 'George and Ursula now have a son, George Junior, so Ursula\'s mother arrives to try and take them back',
        title: 'George of the Jungle 2',
      }
    ],
    total_pages: 1,
    total_results: 1
  };


});
