import { Component, OnInit } from '@angular/core';
import {SerachService} from '../serach.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  constructor(private searchService: SerachService,
              private activatedRoute: ActivatedRoute
  ) {
  }

  // #Lifecycle hooks - When any instance of this component is created it will need to be initially initialized.
  //   That is part of its lifecycle.  When that part of its lifecycle
  //                       occurs I have code that will run.
  //                       There are many other lifecycle hooks to a component
  //                       for example any time something change to the component or when the
  //                        instance of the component is destroyed
  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe(
      routerParam => {
        this.searchService.searchPeople(routerParam.get('query'),
          routerParam.get('adult') === 'true',
          routerParam.get('page'));
      }
    );
  }

}
