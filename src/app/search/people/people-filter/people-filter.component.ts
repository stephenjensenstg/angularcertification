import { Component, OnInit } from '@angular/core';
import {SerachService} from '../../serach.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-people-filter',
  templateUrl: './people-filter.component.html',
  styleUrls: ['./people-filter.component.scss']
})
export class PeopleFilterComponent implements OnInit {

  constructor(private searchService: SerachService,
              private router: Router) {
  }

  peopleFilterForm = new FormGroup(  // #Form This is a reactive Form
    {
      query: new FormControl('', {
        validators: Validators.required   // #Validator for my form.  This will require that this field is populated before submitting.
      }),
      adult: new FormControl(''),
    }
  );

  ngOnInit() {
    this.searchService.people.subscribe(people => {
      const criteria = people && people.criteria;
      this.peopleFilterForm.controls['query'].setValue(criteria.query);
      this.peopleFilterForm.controls['adult'].setValue(criteria.adult);
    });
  }

  submit() {
    this.router.navigate(['search/people'], {queryParams: {
        query: this.peopleFilterForm.controls.query.value,
        adult: this.peopleFilterForm.controls.adult.value,
        page: '1'
      }});
  }
}
