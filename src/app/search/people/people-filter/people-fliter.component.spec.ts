import {ComponentFixture, TestBed} from '@angular/core/testing';
import {PeopleFilterComponent} from './people-filter.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../global/materials-module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Router} from '@angular/router';
import {SerachService} from '../../serach.service';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';


class RouterStub {
  navigate() {
    return ;
  }

}

describe('people filter tests', () => {
  let fixture: ComponentFixture<PeopleFilterComponent>;
  let instance;

  beforeEach(() => {
    TestBed.configureTestingModule( {
      declarations: [
        PeopleFilterComponent
      ],
      imports: [FormsModule, ReactiveFormsModule, MaterialModule, HttpClientTestingModule, BrowserAnimationsModule, NoopAnimationsModule
      ],
      providers: [ SerachService,
        { provide: Router, useClass: RouterStub }
      ],
    });
    fixture = TestBed.createComponent(PeopleFilterComponent);
    fixture.debugElement.injector.get(Router);
    instance = fixture.componentInstance;

  });

  it('should be created', () => {
    const component = TestBed.createComponent(PeopleFilterComponent);
    expect(component).toBeTruthy();
  });

  it('submit call navigate', () => {
    spyOn(instance.router, 'navigate');
    instance.peopleFilterForm.controls.query.setValue('aaa');
    instance.peopleFilterForm.controls.adult.setValue(true);
    fixture.detectChanges();
    instance.submit();
    instance.peopleFilterForm.controls.query.setValue('again');
    instance.peopleFilterForm.controls.adult.setValue(false);
    fixture.detectChanges();
    instance.submit();
    expect(instance.router.navigate).toHaveBeenCalledTimes(2);

  });


});
