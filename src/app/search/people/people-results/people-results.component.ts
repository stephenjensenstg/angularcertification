import { Component, OnInit } from '@angular/core';
import {ICriteriaPerson, SerachService} from '../../serach.service';
import {Router} from '@angular/router';
import {PageEvent} from '@angular/material/paginator';
import {ConstantsService} from '../../../global/constants.service';

@Component({
  selector: 'app-people-results',
  templateUrl: './people-results.component.html',
  styleUrls: ['./people-results.component.scss']
})
export class PeopleResultsComponent implements OnInit {

  constructor(private searchService: SerachService, private router: Router, private constants: ConstantsService) { }

  // pageSize = this.constants.pageSize;

  private criteria: ICriteriaPerson;
  people$ = this.searchService.people;
  pageEvent: PageEvent;

  ngOnInit() {
    this.searchService.people.subscribe(people => {
      this.criteria = people && people.criteria;
    });
  }

  getData($event: PageEvent): PageEvent {
    this.router.navigate(['search/people'], {queryParams: {
        query: this.criteria.query,
        adult: this.criteria.adult,
        page: ($event.pageIndex + 1).toString()
      }});
    return $event;
  }
}
