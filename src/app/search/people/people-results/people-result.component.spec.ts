import {ComponentFixture, TestBed} from '@angular/core/testing';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRouterModule} from '../../../app-router.module';
import {PeopleResultsComponent} from './people-results.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MaterialModule} from '../../../global/materials-module';
import {SerachService} from '../../serach.service';
import {CommonModule} from '@angular/common';
import {ConstantsService} from '../../../global/constants.service';

describe('people results', () => {

  let fixture: ComponentFixture<PeopleResultsComponent>;
  let instance;

  beforeEach(() => {
    TestBed.configureTestingModule({

        declarations: [
          PeopleResultsComponent
        ],
        imports: [
          BrowserModule,
          CommonModule,
          HttpClientTestingModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          FormsModule,
          MaterialModule,
          AppRouterModule,
        ],
        providers: [ SerachService,
          ConstantsService,
        ],
    });

    fixture = TestBed.createComponent(PeopleResultsComponent);
    instance = fixture.componentInstance;
  });

  it('should be able to create component', () => {
    const component = TestBed.createComponent(PeopleResultsComponent);
    expect(component).toBeTruthy();
  });



});
