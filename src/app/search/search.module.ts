import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieComponent } from './movie/movie.component';
import { MovieFilterComponent } from './movie/movie-filter/movie-filter.component';
import { MovieResultsComponent } from './movie/movie-results/movie-results.component';
import {RouterModule} from '@angular/router';
import { PeopleComponent } from './people/people.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../global/materials-module';
import { PeopleFilterComponent } from './people/people-filter/people-filter.component';
import { PeopleResultsComponent } from './people/people-results/people-results.component';

@NgModule({
  declarations: [MovieComponent, MovieFilterComponent, MovieResultsComponent, PeopleComponent, PeopleFilterComponent,
    PeopleResultsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
        {path: 'search/movie', component: MovieComponent},
        {path: 'search/people', component: PeopleComponent}
      ]
    ),
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class SearchModule { }
