import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MovieFilterComponent} from './movie-filter.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../global/materials-module';
import {BrowserModule, By} from '@angular/platform-browser';
import {SerachService} from '../../serach.service';
import {HttpClientModule} from '@angular/common/http';
import {Router} from '@angular/router';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';

class RouterStub {
  navigate() {
    return ;
  }

}

describe('movie filter tests', () => {
  let fixture: ComponentFixture<MovieFilterComponent>;
  let instance;
  let router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, BrowserModule,
        ReactiveFormsModule, MaterialModule,
        HttpClientModule, NoopAnimationsModule, BrowserAnimationsModule ],
      declarations: [MovieFilterComponent],
      providers: [SerachService,
        { provide: Router, useClass: RouterStub}
      ]
    });
    fixture = TestBed.createComponent(MovieFilterComponent);
    router = fixture.debugElement.injector.get(Router);
    instance = fixture.componentInstance;
    instance.ngOnInit();

  });

  it('should be created', () => {
    const component = TestBed.createComponent(MovieFilterComponent);
    expect(component).toBeTruthy();
  });

  // Test what was submitted to router url
  it('query param are set', () => {
    const query = 'George';
    const year = '2005';
    instance.movieFilterForm.controls.query.setValue(query);
    instance.movieFilterForm.controls.year.setValue(year);
    const submitted = instance.submit();
    expect(submitted['queryParams']['query'] === query);
    expect(submitted['queryParams']['year'] === year);
  });

  it('submit call navigate', () => {
    spyOn(instance.router, 'navigate');
    instance.movieFilterForm.controls.query.setValue('aaa');
    instance.movieFilterForm.controls.year.setValue(2005);
    fixture.detectChanges();
    instance.submit();
    instance.movieFilterForm.controls.query.setValue('again');
    instance.movieFilterForm.controls.year.setValue(2003);
    fixture.detectChanges();
    instance.submit();
    expect(instance.router.navigate).toHaveBeenCalledTimes(2);

  });


});
