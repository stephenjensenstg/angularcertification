import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SerachService} from '../../serach.service';
import {Router} from '@angular/router';

// #Component  Ties the template, styling and code together.  Another user then would use the component you built through
//   the selector name you give it.
@Component({
  selector: 'app-movie-filter',
  templateUrl: './movie-filter.component.html',
  styleUrls: ['./movie-filter.component.scss']
})
export class MovieFilterComponent implements OnInit {

  constructor(private searchService: SerachService,
              private router: Router) {
  }

  movieFilterForm = new FormGroup(
    {
      query: new FormControl('', {
        validators: Validators.required
      }),
      year: new FormControl(''),
    }
  );

  ngOnInit() {
    this.searchService.movies.subscribe(movies => {
      const criteria = movies && movies.criteria;
      this.movieFilterForm.controls['query'].setValue(criteria.query);
      this.movieFilterForm.controls['year'].setValue(criteria.year);
    });
  }


  submit(): any  {
    const submitValue = { queryParams: {
        query: this.movieFilterForm.controls.query.value,
        year: this.movieFilterForm.controls.year.value,
        page: '1'
      }};
    this.router.navigate(['search/movie'], submitValue);
    return submitValue;
  }

}
