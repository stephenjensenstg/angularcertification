import { Component, OnInit } from '@angular/core';
import {SerachService} from '../serach.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  constructor(private searchService: SerachService,
              private activatedRoute: ActivatedRoute
              ) {
  }

  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe(
      r => {
        this.searchService.searchMovies(r.get('query'), r.get('year'), r.get('page'));
      }
    );
  }
}
