import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MovieComponent} from '../movie.component';
import {MovieFilterComponent} from '../movie-filter/movie-filter.component';
import {MovieResultsComponent} from './movie-results.component';
import {PeopleComponent} from '../../people/people.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../global/materials-module';
import {PeopleResultsComponent} from '../../people/people-results/people-results.component';
import {RouterTestingModule} from '@angular/router/testing';
import {PeopleFilterComponent} from '../../people/people-filter/people-filter.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('movie results', () => {

  let fixture: ComponentFixture<MovieResultsComponent>;
  let component: MovieResultsComponent;

  beforeEach( () => {
    TestBed.configureTestingModule( {
      declarations: [MovieComponent, MovieFilterComponent, MovieResultsComponent, PeopleComponent,
                      PeopleFilterComponent, PeopleResultsComponent],
      imports: [
        ReactiveFormsModule,
        MaterialModule,
        RouterTestingModule,
        HttpClientTestingModule

      ]
    });

    fixture = TestBed.createComponent(MovieResultsComponent);
    component = fixture.componentInstance;
  });

  it('should be able to create component', () => {
    expect(fixture).toBeTruthy();
  });



});
