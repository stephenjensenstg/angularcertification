import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ICriteriaMovie, IMovieResults, SerachService} from '../../serach.service';
import {PageEvent} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {ConstantsService} from '../../../global/constants.service';
import {tap} from 'rxjs/operators';


@Component({
  selector: 'app-movie-results',
  templateUrl: './movie-results.component.html',
  styleUrls: ['./movie-results.component.scss']
})
export class MovieResultsComponent {

  constructor(private searchService: SerachService,
              private router: Router,
              private constants: ConstantsService) { }

  pageSize = this.constants.pageSize;

  private criteria: ICriteriaMovie;
  // Example of using Behavior Observable.
  // Note this is just a pipe. Inside the HTML I use an async to subscribe.
  movies$ = this.searchService.movies.pipe(
    tap(data => {
      this.criteria = data && data.criteria;
    })
  );
  pageEvent: PageEvent;

  // Using the url to change criteria.  Within result I am navigating to other pages
  getData($event: PageEvent): PageEvent {
    this.router.navigate(['search/movie'], {queryParams: {
         query: this.criteria.query,
         year: this.criteria.year,
         page: ($event.pageIndex + 1).toString()
      }});
    return $event;
  }

  expand(movie: IMovieResults) {
    movie.expanded = !movie.expanded;
  }

}
