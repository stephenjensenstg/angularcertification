import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

// #Service This is functionality that can be injected where needed.
@Injectable({
  providedIn: 'root'
})
export class SerachService {

  private KEY_TOTAL_RESULTS = 'total_results';
  private KEY_RESULTS = 'results';
  private KEY_PAGE = 'page';



  movies = new BehaviorSubject<IMovies>({
    movieResults: [],
    searchInfo: {page: 0, total: 0},
    criteria: {query: '', year: ''}
  });

  people = new BehaviorSubject<IPerson>({
    personResults: [],
    searchInfo: {page: 0, total: 0},
    criteria: {query: '', adult: false}
  });

  constructor(private http: HttpClient) {
  }

  searchMovies(query: string, year: string, page: string) {
    if (query === null) {
      return;
    }
    let params = new HttpParams()
      .set('page', page);
    if (query.length > 0) {
      params = params.append('query', query);
    }
    if (year.length > 3) {
      params = params.append('year', year);
    }
    const results = this.getMovies('https://api.themoviedb.org/3/search/movie', params);
    results.subscribe(data => {
        this.movies.next(
          data[this.KEY_TOTAL_RESULTS] > 0 ? this.movieSearchResults(data, query, year) : null
        );
      }
    );
  }

  searchPeople(query: string, adult: boolean, page: string) {
    if (query === null) {
      return;
    }
    let params = new HttpParams()
      .set('page', page);
    if (query.length > 0) {
      params = params.append('query', query);
    }
    if (adult) {
      params = params.append('include_adult', 'true');
    } else {
      params = params.append('include_adult', 'false');
    }
    const results = this.getPeople('https://api.themoviedb.org/3/search/person', params);
    results.subscribe(data => {
        this.people.next(
          data[this.KEY_TOTAL_RESULTS] > 0 ? this.peopleSearchResults(data, query, adult) : null
        );
      }
    );
  }


  private getMovies(url, params): Observable<IApiMovieSearch> {
    return this.http.get<IApiMovieSearch>(url, {params}).pipe(
      map(data => {
        data.results.map(d => {
          d.expanded = false;
          return d;
        });
        return data;
      })
    );
  }

  private getPeople(url, params): Observable<IPersonResult> {
    return this.http.get<IPersonResult>(url, {params});
  }


  private movieSearchResults(data: IApiMovieSearch, query: string, year: string) {
    return {
      movieResults: data[this.KEY_RESULTS],
      searchInfo:
        {
          page: data[this.KEY_PAGE] - 1,
          total: data[this.KEY_TOTAL_RESULTS]
        },
      criteria: {
        query: query,
        year: year
      }
    };
  }

  private peopleSearchResults(data: IPersonResult, query: string, adult: boolean) {
    return {
      personResults: data[this.KEY_RESULTS],
      searchInfo:
        {
          page: data[this.KEY_PAGE] - 1,
          total: data[this.KEY_TOTAL_RESULTS]
        },
      criteria: {
        query: query,
        adult: adult
      }
    };
  }


}

export interface ISearchInfo {
  page?: number;
  total?: number;
}

export interface ICriteriaMovie {
  query: string;
  year: string;
}

export interface ICriteriaPerson {
  query: string;
  adult: boolean;
}

export interface IMovieResults {
  id?: number;
  adult?: boolean;
  backdrop_path?: string;
  genre_ids?: [];
  original_language?: string;
  original_title?: string;
  overview?: string;
  popularity?: number;
  poster_path?: string;
  release_date?: string;
  title?: string;
  video?: boolean;
  vote_average?: number;
  vote_count?: number;
  expanded?: boolean;
}

export interface IMovies {
  movieResults: IMovieResults[];
  searchInfo: ISearchInfo;
  criteria: ICriteriaMovie;
}

export interface IPerson {
  personResults: IPersonResult[];
  searchInfo: ISearchInfo;
  criteria: ICriteriaPerson;
}

export interface IPersonResult {
  'adult': boolean;
  'gender': number;
  'id': number;
  'known_for': IPersonKnownFor[];
  'known_for_department': string;
  'name': string;
  'popularity': number;
  'profile_path': string;
}

export interface IApiMovieSearch {
  page: number;
  results: IMovieResults[];
  total_pages: number;
  total_results: number;
}

export interface IPersonKnownFor {
  'adult': boolean;
  'backdrop_path': string;
  'id': number;
  'media_type': string;
  'original_language': string;
  'original_title': string;
  'overview': string;
  'poster_path': string;
  'release_date': string;
  'title': string;
  'video': boolean;
  'vote_average': number;
  'vote_count': number;
}

