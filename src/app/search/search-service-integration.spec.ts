import { TestBed } from '@angular/core/testing';
import { SerachService } from './serach.service';
import {HttpClientModule} from '@angular/common/http';
import {AppModule} from '../app.module';
import {delay} from 'rxjs/operators';
import {RouterTestingModule} from '@angular/router/testing';
import {APP_BASE_HREF} from '@angular/common';

describe('SearchService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule, AppModule, RouterTestingModule],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}],
    })
  );

  it('integration with API test search for War', () => {
    const service: SerachService = TestBed.inject(SerachService);
    const FIRST = 1;
    const SECOND = 2;
    const THIRD = 3;
    const FIRSTPAGE = 0;
    const THIRDPAGE = 2;
    let count = 0;
    const QUERY = 'War';
    const YEAR = '2005';
    service.movies.subscribe(data => {
      count++;
      if (count === FIRST) {
        expect(data.movieResults.length).toBe(0);
        expect(data.criteria.query).toBe('');
        expect(data.criteria.year).toBe('');
      }
      if (count === SECOND) {
        expect(data.movieResults.length).toBe(20);
        expect(data.criteria.query).toBe(QUERY);
        expect(data.criteria.year).toBe(YEAR);
        expect(data.searchInfo.page).toBe(FIRSTPAGE);
      }
      if (count === THIRD) {
        expect(data.movieResults.length).toBe(20);
        expect(data.criteria.query).toBe(QUERY);
        expect(data.criteria.year).toBe(YEAR);
        expect(data.searchInfo.page).toBe(THIRDPAGE);
      }
    });

    service.searchMovies(QUERY, YEAR, '1');
    service.searchMovies(QUERY, YEAR, '3');
  });

});
