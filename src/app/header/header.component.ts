import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {ConstantsService} from '../global/constants.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  constructor(private auth: AuthService, private global: ConstantsService, private route: Router) {
  }

  userSubject = this.auth.userSubject;
  user: string;

  ngOnInit(): void {
    this.user = this.global.user == null ? null : this.global.user.userName;
    this.auth.userSubject.subscribe( usr =>  {
      if (usr != null) {
        this.user = usr.userName;
      }
    });

    if (this.user == null) {
      this.route.navigateByUrl('welcome');
    }

  }

  logout() {
    this.auth.logout();
    window.location.href = '/';
  }

  login() {
    this.route.navigateByUrl('login');
  }


}
