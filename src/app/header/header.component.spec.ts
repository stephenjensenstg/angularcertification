import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HeaderComponent} from './header.component';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../global/materials-module';
import {AppRouterModule} from '../app-router.module';
import {ConstantsService} from '../global/constants.service';
import {AuthService} from '../auth/auth.service';

describe( 'header should work', () => {

  beforeEach(() => {

    let fixture: ComponentFixture<HeaderComponent>;

    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent
      ],
      imports: [
        BrowserModule,
        CommonModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        FormsModule,
        MaterialModule,
        AppRouterModule,
      ],
      providers: [ AuthService,
        ConstantsService,
      ],
    });

    fixture = TestBed.createComponent(HeaderComponent);

    it('Should be able to create', () => {
      expect(fixture).toBeTruthy();
    });
  });

});
