import {IMovieDetails} from '../details/details.service';

export class MockMovieDetailService {

  static getMock(id: number, title: string): IMovieDetails {
    const data: IMovieDetails = {};
    data.id = id;
    data.title = title;
    data.overview = 'this is an overview';
    data.production_companies = [
      {
        id: 123,
        name: 'production company 1'
      },
      {
        id: 567,
        name: 'production company 2'
      }
    ];
    return data;
  }

}
