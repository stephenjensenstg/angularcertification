import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  static API_KEY = '88ae69e5b2e07ce2ce99a82e20b13732';

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
        const apiAddedReq = req.clone({
          params: req.params.set('api_key', HttpInterceptorService.API_KEY)
        });
        return next.handle(apiAddedReq);
    }

}


