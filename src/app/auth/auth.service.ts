import { Injectable } from '@angular/core';
import {BackendService} from '../fake/service/backend.service';
import {UserDataType} from '../data/user-data-type';
import {ConstantsService} from '../global/constants.service';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // #Observable A subject is also an Observer and is good one for and example of what and observer is.
  //   This userSubject will be subscribed to throughout the app.
  //   This subscription last.  Whereever there is a subscription they will be fired if anything changes
  //   In the case of this subject that will be for the life of the application.  We can add something to the stream to
  //   be observed by using next.
  //   In the case of other observables it will be a stream of data.  Parts of code will subscribe to get everything
  //   from that stream of data.
  //   That stream of data can by async and a long time.  The subscription last until the stream ends.  This is used in
  //   angular for async operations instead of simple promises
  //   because they have the additional value of additional async operations and not just as single operation that will
  //   run.  That operation can run multiple times as that stream last.
  public userSubject = new BehaviorSubject<UserDataType>(null);

  constructor(private  backend: BackendService, private global: ConstantsService) {
  }

  public login(usr: string, psw: string) {
    let user: UserDataType = null;
    this.backend.login(usr, psw).subscribe( nextUser => {
         user = nextUser;
       },
     error => {
        // console.log(`Could not login User ${usr} with password: ${psw}.  Error: ${error}`);
      },
       () => {
          if (user == null) {
            this.global.user = {userName: '', password: '', id: -999};
            this.userSubject.next(null);
          } else {
            this.global.user = user;
            this.userSubject.next(user);
          }

       }

     );
  }

  public logout() {
    this.global.user = null;
    this.userSubject.next(null);
  }

}
