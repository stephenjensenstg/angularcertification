import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../auth.service';
import {ConstantsService} from '../../global/constants.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {UserDataType} from '../../data/user-data-type';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm = new FormGroup( {
    user: new FormControl(''),
    password: new FormControl(),
  });

  private subscriptionUser;

  public loginValid = false;

  private userDataTypeObservable: Observable<UserDataType>;

  constructor( private auth: AuthService, private global: ConstantsService, private route: Router) {
    this.userDataTypeObservable = this.auth.userSubject;
  }

  ngOnInit() {
    this.subscriptionUser = this.userDataTypeObservable.subscribe(user => {
      if (user != null) {
        if (this.global.user != null) {
          this.route.navigateByUrl('dashboard');
        }
      } else {
          if (this.global.user != null && this.global.user.id === this.global.INVALID_USER) {
            alert('Invalid Username or Password');
            this.global.user = null;
          }
          this.route.navigateByUrl('login');
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptionUser.unsubscribe();
  }

  loginUser() {
    if (this.validEntry() === true) {
      this.auth.login(this.loginForm.controls.user.value,
                      this.loginForm.controls.password.value);
    }
    this.loginForm.controls.user.setValue('');
    this.loginForm.controls.password.setValue('');

  }

  change() {
    if (this.validEntry() === true) {
      this.loginValid = true;
    } else {
      this.loginValid = false;
    }
  }

  validEntry(): boolean {
    let retval = false;
    const user = this.loginForm.controls.user;
    const password = this.loginForm.controls.password;
    if ( user != null && user.value.toString().trim().length > 0 &&
        password != null &&
      password.value !== null &&
      password.value.toString().trim().length > 0
    ) {
      retval = true;
    }
    return retval;
  }

}
