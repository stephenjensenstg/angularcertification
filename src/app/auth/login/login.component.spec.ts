import {ComponentFixture, TestBed} from '@angular/core/testing';
import {LoginComponent} from './login.component';
import {AuthService} from '../auth.service';
import {ConstantsService} from '../../global/constants.service';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../global/materials-module';

describe('login page', () => {
  let fixture: ComponentFixture<LoginComponent>;

  class RouterStub {
    navigateByUrl() {
      return;
    }
  }

  beforeEach( () => {

    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule

      ],
      providers: [
        AuthService,
        ConstantsService
      ]
    });

    fixture = TestBed.createComponent(LoginComponent);


  });

  it('should be able to create', () => {
    debugger;
    expect(fixture).toBeTruthy()
  });

});
