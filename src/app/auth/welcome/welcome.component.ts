import { Component, OnInit } from '@angular/core';
import {Router, RouterLink} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(private auth: AuthService, private route: Router) { }

  public userSubject = this.auth.userSubject;

  ngOnInit() {
  }


  login() {
    this.route.navigateByUrl('login');
  }
}
