import {ComponentFixture, TestBed} from '@angular/core/testing';
import {WelcomeComponent} from './welcome.component';
import {Router, RouterModule} from '@angular/router';

describe('welcome page', () => {

  let fixture: ComponentFixture<WelcomeComponent>;

  class RouterStub {
    navigateByUrl() {
      return;
    }
  }

  beforeEach(() => {

    TestBed.configureTestingModule({

      declarations: [
        WelcomeComponent
      ],
      imports: [
        RouterModule
      ],
      providers: [
        { provide: Router, useClass: RouterStub }
      ]
    });

    fixture = TestBed.createComponent(WelcomeComponent);
  });

  it('Should be able to create', () => {
    expect(fixture).toBeTruthy();
  });

});
