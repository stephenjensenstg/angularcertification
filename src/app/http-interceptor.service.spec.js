"use strict";
exports.__esModule = true;
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/common/http/testing");
var http_interceptor_service_1 = require("./http-interceptor.service");
describe('Interceptor for http request can be created', function () {
    beforeEach(function () {
        return testing_1.TestBed.configureTestingModule({
            imports: [testing_2.HttpClientTestingModule]
        });
    });
    it('should be created', function () {
        var service = testing_1.TestBed.get(http_interceptor_service_1.HttpInterceptorService);
        expect(service).toBeTruthy();
    });
});
//# sourceMappingURL=http-interceptor.service.spec.js.map