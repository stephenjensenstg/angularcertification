import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  declarations: [DashboardComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {path: 'dashboard', component: DashboardComponent},
            {path: '', component: DashboardComponent}
          ]
        ),
        MatIconModule
    ]
})
export class DashboardModule { }
