import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {IFavorite} from '../global/constants.service';
import {StorageService} from '../store/storage.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})

// #interface  Example implementing an interface.  Because I am implementing this interface I must implement it as OnInit
//   wants to be implemented in this case I need to implement a method ngOnInit.  This is often referred to as fullfilling the contract.
export class DashboardComponent implements OnInit {
  favorites$: Observable<IFavorite[]> = this.storage.favorites$;

  constructor(private loginService: AuthService,
              private router: Router,
              private storage: StorageService) { }



  ngOnInit(): void {
    this.storage.getStoredFavorites();
  }

  toggleDescription(favorite: IFavorite)  {
    favorite.show = !favorite.show;
  }

  removeFavorite(favorite: IFavorite) {
      this.storage.removeFavorite(favorite);
  }
}
