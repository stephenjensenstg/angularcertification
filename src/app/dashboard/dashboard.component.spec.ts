import {ComponentFixture, TestBed} from '@angular/core/testing';
import {DashboardComponent} from './dashboard.component';
import {IFavorite} from '../global/constants.service';
import {StorageService} from '../store/storage.service';
import {MockMovieDetailService} from '../mocks/mock-movie-detail-service';
import {RouterTestingModule} from '@angular/router/testing';
import {MatIconModule} from '@angular/material/icon';
import {BehaviorSubject, of} from 'rxjs';

describe('dashboard', () => {
  let fixture: ComponentFixture<DashboardComponent>;
  const mockData1 = MockMovieDetailService.getMock(20, 'My Title');
  const mockData2 = MockMovieDetailService.getMock(40, 'another header');
  let favoritesList: IFavorite[] = [];
  favoritesList.push({id: mockData1.id, header: mockData1.title, description: mockData1.overview});
  favoritesList.push({id: mockData2.id, header: mockData2.title, description: mockData2.overview});

  const mockStorageService = {
    favorites$: new BehaviorSubject<IFavorite[]>(favoritesList),
    getStoredFavorites: (): IFavorite[] => {
      return favoritesList;
    },
    setStoredMovies: (movies: IFavorite[]) => {
      favoritesList = movies;
    }
  };

  beforeEach(() => {

    TestBed.configureTestingModule( {
      declarations: [DashboardComponent],
      imports: [
        RouterTestingModule,
        MatIconModule
      ],
      providers: [
        {provide: StorageService, useValue: mockStorageService},
      ]
    }).compileComponents();
  });
  it( 'should render', (done) => {
      fixture = TestBed.createComponent(DashboardComponent);
      fixture.detectChanges();
      fixture.componentInstance.favorites$.subscribe(data => {
        expect(data.length).toBe(favoritesList.length);
        done();
      });
    });

  }
);
