export interface UserDataType {
  id: number;
  userName: string;
  password: string;
}
