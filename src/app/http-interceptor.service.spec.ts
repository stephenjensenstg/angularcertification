import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { HttpInterceptorService } from './http-interceptor.service';

describe('Interceptor for http request can be created', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
  );

  it('should be created', () => {
    const service: HttpInterceptorService = TestBed.inject(HttpInterceptorService);
    expect(service).toBeTruthy();
  });
});
