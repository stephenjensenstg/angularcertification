import { Injectable } from '@angular/core';
import {UserDataType} from '../data/user-data-type';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
  INVALID_USER = -999;
  STORED_MOVIE_IDS = 'storeMovie';
  pageSize = 20;
  numberOfCastingCount = 5;
  numberOfCrewCount = 5;
  constructor() { }
  user: UserDataType;

}

export interface IFavorite {
  id?: number;
  type?: string;
  header?: string;
  poster?: string;
  description?: string;
  show?: boolean;
  adult?: boolean;
}
