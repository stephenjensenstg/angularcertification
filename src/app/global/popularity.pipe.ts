import { Pipe, PipeTransform } from '@angular/core';

export const popUndetermined = 'undetermined';

export const popNotPopular = 'Currently not Popular';

export const popAverage = 'Average';

export const popAbove = 'Above Average';

export const popHigh = 'High Popularity';

export const popExtreme = 'Extremely Popular';

@Pipe({
  name: 'popularity'
})
export class PopularityPipe implements PipeTransform {

  transform( num: any, ...args: any[] ): string {
    let retval = popUndetermined;
    if (num == null) {
      retval = popUndetermined;
    } else if (num < 2) {
      retval = popNotPopular;
    } else if (num >= 2 && num < 9) {
      retval = popAverage;
    } else if (num >= 9  && num < 20) {
      retval = popAbove;
    } else if (num >= 20  && num < 40) {
      retval = popHigh;
    } else if (num >= 40) {
      retval = popExtreme;
    }
    return retval;
  }

}
