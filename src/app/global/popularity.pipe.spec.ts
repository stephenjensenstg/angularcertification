import {popAverage, popExtreme, popNotPopular, popUndetermined, PopularityPipe} from './popularity.pipe';

describe('PopularityPipe', () => {
  it('create an instance', () => {
    const pipe = new PopularityPipe();
    expect(pipe).toBeTruthy();
  });
  it('a score below 2 is not popular', () => {
    const pipe = new PopularityPipe();
    expect(pipe.transform(1)).toBe(popNotPopular);
  });
  it('a score above 40 is extremely popular', () => {
    const pipe = new PopularityPipe();
    expect(pipe.transform(42)).toBe(popExtreme);
  });
  it('a score of 5 is average', () => {
    const pipe = new PopularityPipe();
    expect(pipe.transform(8)).toBe(popAverage);
  });
  it('a non value score is undetermined', () => {
    const pipe = new PopularityPipe();
    expect(pipe.transform('ee')).toBe(popUndetermined);
  });
  it('a null value is undetermined', () => {
    const pipe = new PopularityPipe();
    expect(pipe.transform(null)).toBe(popUndetermined);
  });
  it('a undefined value is undetermined', () => {
    const pipe = new PopularityPipe();
    expect(pipe.transform(undefined)).toBe(popUndetermined);
  });

});
