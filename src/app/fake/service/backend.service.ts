import { Injectable } from '@angular/core';
import {UserDataType} from '../../data/user-data-type';
import {from, Observable} from 'rxjs';
import {delay, filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private userJsonPath = 'assets/users.json';
  private userData: UserDataType[];

  constructor() {
    fetch(this.userJsonPath)
      .then(response => response.json())
      .then(d => {
        this.userData = d;
    });

  }

  // #pipe there is also this type of pipe but I don't think that is what you wanted (See other pipe explanation)
  //   With observers you may need to provide additional computation.  We think of doing this computation within a
  //   stream of data.  Continuing with that metaphor we can provide a pipe into that stream where do operations and
  //   then let it continue within that stream. These operations are done within a pipe.
  //   This is different then a subscribe.  subscribe is the endpoint that will need to do something with the stream of
  //    data going through the pipe.  Also you need something to subscribe to an observer for anything to be initiated.
  //    unlike subscribe a pipe does not initiate anything and can be placed anywhere you want to manipulate that stream.
  getAllUser(): Observable<string> {
    return from(this.userData).pipe(
       delay(1000),
       map(d => d.userName)
    );
  }

  // Make this act like a server by adding a delay
  login(name: string, password: string): Observable<UserDataType> {
     return from(this.userData).pipe(
       delay(1000),
       filter( d => (d.userName.toLowerCase() === name.toLowerCase() && d.password === password))
     );
  }

}
