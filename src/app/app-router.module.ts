import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot([
    { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
        // canActivate: ['soemthing here']  // #Guard this would of been a great place to have provided security to
        // dashboard, but did not know about it now I do.
    }
], { relativeLinkResolution: 'legacy' }),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
